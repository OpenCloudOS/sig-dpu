## 【DPU SIG】
## 职责范围 Scope
OpenCloudOS与DPU在两大场景中需要紧密协同工作，
- 场景一、OpenCloudOS作为主机侧操作系统，DPU需要将所提供的软硬件能力暴露给OpenCloudOS，供操作系统自身或上层应用使用；
- 场景二、OpenCloudOS作为DPU的操作系统，通过与DPU硬件相结合提供给DPU使用者自有应用部署环境及DPU应用开发环境。

DPU SIG主要职责是在上述两种场景中，进行OpenCloudOS与DPU间软硬件互联方式，使用接口，驱动集成等方面的标准或建议使用方式的制定与开发，组织并协调小组成员共同推进OpenCloudOS与DPU共有软件生态的打造

## SIG成员 Members
- bart（腾讯）
- 王亮（腾讯）
- 余曦（大禹智芯）
## 联络方式 Contact
- 微信群 （可选）
- 邮件列表
  - 小组邮件列表
    -  SIG-DPU@lists.opencloudos.org
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：（请填写形式及时间）
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献
## SIG规划 Roadmap
- DPU厂商驱动与OpenCloudOS的集成
- 为DPU操作系统定制精简版OpenCloudOS

